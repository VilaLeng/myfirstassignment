import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang Campus",
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.camera_alt),
          title: Text('The Win World Wide App', style: TextStyle(
            color: Colors.yellow,
          ),),
          actions: [IconButton(onPressed: (){}, icon: Icon(Icons.radio_button_checked))],
        ),
        body: Center(
          child: Text('Welcome to the W3A',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 35,
            color: Colors.redAccent,
          ),),

        ),
      ),
    );


  }
}
